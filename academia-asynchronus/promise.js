 

 const posts = [
   {
     title: "Post one",
     body: "This is post one"
   },
   {
     title: "Post two",
     body: "This is post two"
   }
 ]
 
 const createPost = post => {
   return new Promise((resolve, reject) => {
     setTimeout(() => {
       posts.push(post)
       const error = false
       if(!error) {
         resolve({ msg: 'Data berhasil ditambahkan'})
       }else{
         reject({ msg: 'Data gagal ditambahkan'})
       }
     }, 2000)
   })
 }
 
 const getPosts = () => {
   setTimeout(() => {
     posts.forEach(post => {
       console.log(post)
     })
   }, 1000)
 }
 
 const newPost = {
   title: "Post three",
   body: "This is post three"
 }
 

//  cara 1 asynchronus - callback
// createPost(newPost, getPosts)
// cara 2 asynchronus - callback
//  createPost(newPost, (data)=> {
//    console.log(data)
 
//    getPosts()
//  })

//  cara 3 promise
 createPost(newPost)
  .then(function(data){
      console.log(data)
      getPosts()
  })
  .catch(error => console.log(error))


