// contoh 1
// function myFirst(){
//    console.log('Hello')
// }

// function mySecond(){
//    console.log('Goodbye')
// }

// myFirst()
// mySecond()

// contoh 2
// function callback dan asynchronus
// callback utk mendahulukan manggil fungsi second 
const first = function myFirst( callback ){
   setTimeout(() => {
      console.log('Hello')

      callback()
   }, 2000)
}

const second = function mySecond(){
   console.log('Goodbye')
}

// manggilnya jgn kaya gini
// first(second)

// tapi kaya gini
first( () => {
   second()
})
