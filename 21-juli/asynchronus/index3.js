const first = function myFirst( callback ){
   setTimeout(() => {
      console.log('Hello')

      callback({
         user : {
            nama : 'Kenang Ghalih',
            usia : 20
         }
      })
   }, 2000)
}

const second = function mySecond(){
   console.log('Goodbye')
}

// manggil fungsi dgn es6
first( (data) => {
   console.log(data)
   second()
})
